# Log Dump Service Shut Down

1. [tool3] Rename "/usr/share/webapps/logdump" to "/usr/share/webapps/logdump-ORIGINAL"

3. [tools3] Copy "/usr/lib/cgi-bin/logdump-request" to "/usr/lib/cgi-bin/logdump-request-CHG00083659"

4. [tools3] Copy "/usr/lib/cgi-bin/logdump-request-disabled" to "/usr/lib/cgi-bin/logdump-request"

5. [tools3] Make /usr/lib/cgi-bin/logdump-request-CHG00083659 invisible to Apache by chmod'ing it to "700".

6. [tools] Record the list of jobs that are still enabled in the logdump
database by running this query. Add the results to the Change Task
notes: "mysql -h mysql-app.stanford.edu -u s_www_logdump -D
s_www_logdump -p" The password is in /etc/tools-cgi/logdumpdb. Query:
"select rec_num, requester, name from request_sets where inactive=0;".

7. [tools3] Set all records to be inactive: UPDATE request_sets SET inactive=1 ;

8. Edit the service page at
   https://uit.stanford.edu/service/web/centralhosting and remove the
   paragraph titled "Log Dump and Analysis Service".
